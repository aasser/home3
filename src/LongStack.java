import java.util.LinkedList;

public class LongStack {

  	
   public static void main (String[] argum) {
	   
	   Long jada = interpret("2 -");
	   System.out.println(jada);
	   
   }

   private LinkedList<Long> m6mmi;
   
   LongStack() {  
	   this.m6mmi = new LinkedList<Long>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
	   LongStack kopeeri = new LongStack();
	   
	   for(int i = 0; i < m6mmi.size(); i++){
           kopeeri.m6mmi.add(m6mmi.get(i));
   }                      
   return kopeeri;
   }

   public boolean stEmpty() {
      return m6mmi.isEmpty();
   }

   public void push (long a) {
      m6mmi.push(a);
   }

   public long pop() {
	   if(m6mmi.size() < 1){
           throw new RuntimeException("Liiga vähe arve/numbreid");
      }
      return m6mmi.pop();
   }

   public void op (String s) {
	   
	   if(m6mmi.size() < 2){
           throw new RuntimeException("Liiga vähe arve/numbreid");
        }
	   if(!s.equals("+") && !s.equals("*") && !s.equals("/") && !s.equals("-")){
           throw new RuntimeException("Tehtes on valed tehtemärgid, vaata üle! (lubatud on: +,-,/,* " + s +"." );
        }
	   
	   	long op2 = pop();
	   	long op1 = pop();
	      if (s.equals ("+")) push (op1 + op2);
	      if (s.equals ("-")) push (op1 - op2);
	      if (s.equals ("*")) push (op1 * op2);
	      if (s.equals ("/")) push (op1 / op2);

   }
  
   public long tos() {
	   if (stEmpty())
	         throw new IndexOutOfBoundsException ("magasini alataitumine");
	      return m6mmi.element(); // TODO!!!
   }

   @Override
   public boolean equals (Object o) {
	   
      return m6mmi.equals(((LongStack)o).m6mmi);
   }

   @Override
   public String toString() {
	 if (stEmpty()) return "Tyhi";
	 
	  StringBuffer b = new StringBuffer();
	  

	  	//koodi jupp laenatud https://github.com/egaia/AlgorithmAndDataStructure/blob/master/home3/src/LongStack.java
	  	for(int i = m6mmi.size()-1; i>=0; i--){
	         b.append(m6mmi.get(i));
	         if(i > 0){
	            b.append(' ');
	         }
	      }
	      return b.toString();	

   }
   
   //Meetod interpret ei ütle vea korral, mis oli vigane avaldis tervikuna (neid saab ju programmis olla erinevaid) 
   //ning mis oli vea täpsem põhjus avaldise seisukohalt vaadates.

   public static long interpret (String pol) {
	   
	   //koodi idee laenatud https://github.com/egaia/AlgorithmAndDataStructure/blob/master/home3/src/LongStack.java
	   LongStack longStack = new LongStack();
	   
       String[] pieces = pol.trim().split("\\s+");
       
       int nrCount = 0; //ops
	   int i = 0; //nums
	   
	      if(pol == null | pol.trim().length() == 0 | pol == ""){
	          throw new RuntimeException("Esitatud avaldis, ei tohi olla tühi");
	       }
	      
	   
	      for(String element : pieces){
	          try{
	             longStack.push(Integer.valueOf(element));
	             i++;
	          }catch(NumberFormatException e){
	             if(longStack.stEmpty()){
	                throw new RuntimeException("Tehtemärk ei saa olla avaldise " + pol + " esimene operatsioon");
	             }
	             else if(longStack.m6mmi.size() < 2){
	                throw new RuntimeException("Liiga vähe arve/numbreid avaldises " + pol);
	             }
	             else if(!element.equals("+") && !element.equals("*") && !element.equals("/") && !element.equals("-")){
	                throw new RuntimeException("Avaldises " + pol + " on valed tehtemärgid, vaata üle! (lubatud on: +,-,/,* " + element +"." );
	             }
	             longStack.op(element);
	             nrCount++;
	          }
	       }

	       if(i-1 != nrCount){
	          throw new RuntimeException("Ette andtud andmed avaldises " + pol + " on balansist väljas (integers: " + i + " , operations: " + nrCount+").");
	       }
	       return longStack.pop();
	    } 

   }




